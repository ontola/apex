# frozen_string_literal: true

class CustomFormForm < ContainerNodeForm
  field :display_name
  grants_group
end
