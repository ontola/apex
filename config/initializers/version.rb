# frozen_string_literal: true

VERSION = '157.0.2' unless defined?(::VERSION)
